from flet import *
from flet import Ref
from components.custom_button import CustomButton
from components.show_resize_dialog import showResizeInputsDialog
from components.show_saving_dialog import showInputSavePathDialog
from utils.project_assets import ProjectAssets
from utils.project_globals import ProjectGlobals
from utils.project_size import ProjectSize
from utils.project_colors import ProjectColors
from utils.project_strings import ProjectStrings
from utils.operations_options import MainOperations, OperationsOptions
from functions.resize_image_function import ResizeImageFunction
from functions.filtering_functions import FilteringFactory

class SelectOperationsForm(UserControl):
    def build(self):
        mainContainerRef = Ref[Container]()
        subContainerRef = Ref[Container]()
        mainDropdownRef = Ref[Dropdown]()
        subDropdownRef = Ref[Dropdown]()
        mainContainer = Container(
            ref=mainContainerRef,
            width=ProjectSize.s300,
            padding=ProjectSize.s10,
        )
        self.mainOperation = ""
        self.subOperation = ""

        def onSubOperationChange(e):
            self.subOperation = e.control.value

        subOperationDropdown = buildOperationsDropDown(
            ref=subDropdownRef,
            label=ProjectStrings.subOperation,
            options=[],
            onChange=onSubOperationChange,
        )

        def mainActionWhenSelectOperation():
            ProjectGlobals.SecondImageBoxContainerRef.current.width = 0
            ProjectGlobals.SecondImageBoxContainerRef.current.update()
            if ProjectGlobals.SecondSelectedImage != ProjectAssets.vectorImage:
                ProjectGlobals.SecondBtnRef.current.width = 0
                ProjectGlobals.SecondBtnRef.current.update()

        def onMainOperationChange(e):
            self.mainOperation = e.control.value
            if self.mainOperation == MainOperations.filtering.value:
                subDropdownRef.current.options = OperationsOptions.filteringOperations
            elif self.mainOperation == MainOperations.resizeImage.value:
                subDropdownRef.current.options = []  # Add resize options if any
            subContainerRef.current.content = subOperationDropdown
            subContainerRef.current.update()

        mainOperationsDropdown = buildOperationsDropDown(
            ref=mainDropdownRef,
            label=ProjectStrings.operation,
            options=OperationsOptions.mainOperations,
            onChange=onMainOperationChange
        )

        def startProcessing(e):
            if ProjectGlobals.FirstSelectedImage == ProjectAssets.vectorImage:
                return
            if self.mainOperation == MainOperations.filtering.value:
                FilteringFactory.startProcessing(operation=self.subOperation)
            elif self.mainOperation == MainOperations.resizeImage.value:
                showResizeInputsDialog(self)

        subContainer = Container(ref=subContainerRef)
        mainContainer.content = Column(
            controls=[
                mainOperationsDropdown,
                subContainer,
                CustomButton(
                    icons.PLAY_CIRCLE_FILL_OUTLINED,
                    ProjectStrings.start,
                    lambda _: startProcessing(self),
                ),
                CustomButton(
                    icons.SAVE,
                    ProjectStrings.saveImage,
                    lambda _: showInputSavePathDialog(self),
                ),
            ],
            horizontal_alignment=CrossAxisAlignment.CENTER,
            alignment=MainAxisAlignment.CENTER,
        )
        return Column(
            controls=[
                mainContainer,
                Container(height=ProjectSize.s40),
            ],
            horizontal_alignment=CrossAxisAlignment.CENTER,
        )

def buildOperationsDropDown(label, ref, options, onChange):
    return Dropdown(
        ref=ref,
        width=ProjectSize.s200,
        hint_text=ProjectStrings.selectOperation,
        label=label,
        color=ProjectColors.black,
        label_style=TextStyle(
            color=ProjectColors.black,
        ),
        options=options,
        on_change=onChange
    )

