import numpy as np
import cv2

# Transformation Matrices for RGB to LMS
MsRGB = np.array([[17.8824, 43.5161, 4.1193],
                  [3.4557, 27.1554, 3.8671],
                  [0.02996, 0.18431, 1.4700]])

# LMS to RGB transformation matrix
lmsToRgb = np.array([[0.0809, -0.1305, 0.1167],
                     [-0.0102, 0.0540, -0.1136],
                     [-0.0003, -0.0041, 0.6932]])

# Protanopia transformation matrix
protanopia = np.array([[0, 2.02344, -2.52581],
                       [0, 1, 0],
                       [0, 0, 1]])

# Deuteranopia transformation matrix
deuteranopia = np.array([[1, 0, 0],
                         [0.4942, 0, 1.2483],
                         [0, 0, 1]])

# Tritanopia transformation matrix
tritanopia = np.array([[1, 0, 0],
                       [0, 1, 0],
                       [-0.3959, 0.8011, 0]])

# Combined transformation matrices
Trgb2lms = MsRGB.T
Tlms2rgb = np.linalg.inv(Trgb2lms).T

def apply_cvd(img, cvd_matrix):
    # Reshape the image to a 2D array (3, height * width) for matrix multiplication
    imgReshaped = img.reshape(-1, 3).T

    # Apply RGB to LMS transformation
    imgLMS = Trgb2lms @ imgReshaped

    # Apply color blindness matrix
    imgColorBlindLMS = cvd_matrix @ imgLMS

    # Convert back from LMS to RGB
    imgOUTrgb = lmsToRgb @ imgColorBlindLMS

    # Reshape back to the original image dimensions
    imgOUTrgb = imgOUTrgb.T.reshape(height, width, channels).astype(np.uint8)
    
    return imgOUTrgb

# Load the image
imgpath = "test_pic.jpg"  # Adjust the path accordingly
imgIN = cv2.imread(imgpath, cv2.IMREAD_UNCHANGED)

# Check if image is loaded
if imgIN is None:
    print("Error: Image not loaded. Check the file path.")
else:
    print("Image loaded successfully.")

# Convert the image from BGR to RGB
imgINrgb = cv2.cvtColor(imgIN, cv2.COLOR_BGR2RGB)

# Get image dimensions
height, width, channels = imgINrgb.shape

# Apply different color blindness simulations
imgProtanopia = apply_cvd(imgINrgb, protanopia)
imgDeuteranopia = apply_cvd(imgINrgb, deuteranopia)
imgTritanopia = apply_cvd(imgINrgb, tritanopia)

# Save the original and the transformed images
cv2.imwrite('original_image.jpg', imgIN)
cv2.imwrite('protanopia_image.jpg', cv2.cvtColor(imgProtanopia, cv2.COLOR_RGB2BGR))
cv2.imwrite('deuteranopia_image.jpg', cv2.cvtColor(imgDeuteranopia, cv2.COLOR_RGB2BGR))
cv2.imwrite('tritanopia_image.jpg', cv2.cvtColor(imgTritanopia, cv2.COLOR_RGB2BGR))

print("Images saved successfully.")

