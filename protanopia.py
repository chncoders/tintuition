from PIL import Image
import cv2
import numpy as np

def rgb_to_hsv(r, g, b):
    rgb = np.uint8([[[r, g, b]]])
    hsv = cv2.cvtColor(rgb, cv2.COLOR_RGB2HSV)
    return hsv[0][0][0] / 179.0, hsv[0][0][1] / 255.0, hsv[0][0][2] / 255.0

def hsv_to_rgb(h, s, v):
    hsv = np.uint8([[[h * 179, s * 255, v * 255]]])
    rgb = cv2.cvtColor(hsv, cv2.COLOR_HSV2RGB)
    return int(rgb[0][0][0]), int(rgb[0][0][1]), int(rgb[0][0][2])

def is_dominant_color(r, g, b, dominant_color):
    if dominant_color == "red":
        return r > g and r > b
    elif dominant_color == "green":
        return g > r and g > b
    return False

def adjust_hsl(h, s, l, dominant):
    if dominant:
        h = (h + 0.3) % 1.0  # Adjust hue by 30%
        s = min(s * 1.1, 1.0)  # Increase saturation by 10%
        l = min(l + 0.25, 1.0)  # Increase lightness by 25%
    else:
        s = min(s * 1.1, 1.0)  # Increase saturation by 10%
        l = min(l * 1.1, 1.0)  # Increase lightness by 10%
    return h, s, l

def color_blind_filter(image_path, output_path):
    img = Image.open(image_path)
    img = img.convert('RGB')
    pixels = img.load()
    width, height = img.size

    # Determine the dominant color
    red_count = 0
    green_count = 0
    for y in range(height):
        for x in range(width):
            r, g, b = pixels[x, y]
            if r > g and r > b:
                red_count += 1
            elif g > r and g > b:
                green_count += 1

    dominant_color = "red" if red_count > green_count else "green"

    # Adjust the colors
    for y in range(height):
        for x in range(width):
            r, g, b = pixels[x, y]
            h, s, v = rgb_to_hsv(r, g, b)
            dominant = is_dominant_color(r, g, b, dominant_color)
            h, s, v = adjust_hsl(h, s, v, dominant)
            r, g, b = hsv_to_rgb(h, s, v)
            pixels[x, y] = (r, g, b)

    img.save(output_path)

# Example usage
color_blind_filter("sampletest2.jpg", "sampletest_protan2.jpg")

