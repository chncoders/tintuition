from PIL import Image
import numpy as np

CVDMatrix = {
    "Tritanope": [  # blues are greatly reduced (0.003% population)
        1.0, 0.0, 0.0,
        0.0, 1.0, 0.0,
        -0.395913, 0.801109, 0.0
    ]
}

def daltonize(image_path, amount=1.0):
    cvd = CVDMatrix["Tritanope"]
    cvd_a, cvd_b, cvd_c = cvd[0], cvd[1], cvd[2]
    cvd_d, cvd_e, cvd_f = cvd[3], cvd[4], cvd[5]
    cvd_g, cvd_h, cvd_i = cvd[6], cvd[7], cvd[8]

    img = Image.open(image_path).convert("RGB")
    np_img = np.array(img, dtype=float)

    # Apply Daltonization
    for y in range(np_img.shape[0]):
        for x in range(np_img.shape[1]):
            r, g, b = np_img[y, x, 0], np_img[y, x, 1], np_img[y, x, 2]

            # RGB to LMS matrix conversion
            L = (17.8824 * r) + (43.5161 * g) + (4.11935 * b)
            M = (3.45565 * r) + (27.1554 * g) + (3.86714 * b)
            S = (0.0299566 * r) + (0.184309 * g) + (1.46709 * b)

            # Simulate color blindness
            l = (cvd_a * L) + (cvd_b * M) + (cvd_c * S)
            m = (cvd_d * L) + (cvd_e * M) + (cvd_f * S)
            s = (cvd_g * L) + (cvd_h * M) + (cvd_i * S)

            # LMS to RGB matrix conversion
            R = (0.0809444479 * l) + (-0.130504409 * m) + (0.116721066 * s)
            G = (-0.0102485335 * l) + (0.0540193266 * m) + (-0.113614708 * s)
            B = (-0.000365296938 * l) + (-0.00412161469 * m) + (0.693511405 * s)

            # Isolate invisible colors to color vision deficiency (calculate error matrix)
            R = r - R
            G = g - G
            B = b - B

            # Shift colors towards visible spectrum (apply error modification)
            RR = (0.0 * R) + (0.0 * G) + (0.0 * B)
            GG = (0.7 * R) + (1.0 * G) + (0.0 * B)
            BB = (0.7 * R) + (0.0 * G) + (1.0 * B)

            # Add compensation to original values
            R = RR + r
            G = GG + g
            B = BB + b

            # Clamp values
            R = max(0, min(255, R))
            G = max(0, min(255, G))
            B = max(0, min(255, B))

            # Update pixel values
            np_img[y, x, 0] = R
            np_img[y, x, 1] = G
            np_img[y, x, 2] = B

    # Create a new image from the modified pixel array
    corrected_img = Image.fromarray(np_img.astype('uint8'), 'RGB')
    return corrected_img

# Example usage
corrected_image = daltonize("sampletest2.jpg")
corrected_image.save("sampletest_tritan.png")

