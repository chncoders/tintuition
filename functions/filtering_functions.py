import cv2 as cv
import matplotlib.pyplot as plt

import numpy as np
from PIL import Image, ImageFilter
from functions.save_image_function import updateTheProcessedImage

from utils.project_enums import FilteringOperations
from utils.project_globals import ProjectGlobals


class FilteringFunctions():
    def averageFiltering(self):
        img = cv.imread(ProjectGlobals.FirstSelectedImage)
        # averageBlurImage = cv.blur(img, (5, 5))
        # cv.imshow('After Average Blur', averageBlurImage)
        # updateTheProcessedImage(averageBlurImage)
        # cv.waitKey(0)
        # cv.destroyAllWindows()
        imgWithMoreBlurred = np.hstack([img,
                                        cv.blur(img, (3, 3)),
                                        cv.blur(img, (5, 5)),
                                        cv.blur(img, (7, 7)),
                                        cv.blur(img, (13, 13)),
                                        ])
        cv.imshow('Average Filter', imgWithMoreBlurred)
        updateTheProcessedImage(imgWithMoreBlurred)
        cv.waitKey(0)

    def gaussianFiltering(self):
        img = cv.imread(ProjectGlobals.FirstSelectedImage)
        # gaussianBlurImage = cv.GaussianBlur(img, (5, 5), 10)
        # cv.imshow('After Gaussian Blur', gaussianBlurImage)
        # updateTheProcessedImage(gaussianBlurImage)
        # cv.waitKey(0)
        # cv.destroyAllWindows()
        # apply more than one blur
        imgWithMoreBlurred = np.hstack([img,
                                        cv.GaussianBlur(img, (3, 3), 0),
                                        cv.GaussianBlur(img, (5, 5), 0),
                                        cv.GaussianBlur(img, (7, 7), 0),
                                        cv.GaussianBlur(img, (13, 13), 0),
                                        ])
        cv.imshow('Gaussian Filter', imgWithMoreBlurred)
        updateTheProcessedImage(imgWithMoreBlurred)
        cv.waitKey(0)

    def medianFiltering(self):
        img = cv.imread(ProjectGlobals.FirstSelectedImage, 0)
        # medianBlurImage = cv.medianBlur(img, 5)  # remove salt and pepper noise
        # cv.imshow('After', medianBlurImage)
        # updateTheProcessedImage(medianBlurImage)
        # cv.waitKey(0)
        imgWithMoreBlurred = np.hstack([img,
                                        cv.medianBlur(img, 3),
                                        cv.medianBlur(img, 5),
                                        cv.medianBlur(img, 7),
                                        cv.medianBlur(img, 13),
                                        ])
        cv.imshow('Median Filter', imgWithMoreBlurred)
        updateTheProcessedImage(imgWithMoreBlurred)
        cv.waitKey(0)

    def minFiltering(self):
        img = Image.open(ProjectGlobals.FirstSelectedImage)
        # Remove Salt noise
        minFilter = img.filter(ImageFilter.MinFilter(size=3))
        img.show()
        updateTheProcessedImage(minFilter)
        minFilter.show(title='Min Filter Image')

    def maxFiltering(self):
        img = Image.open(ProjectGlobals.FirstSelectedImage)
        # Remove Pepper noise
        maxFilter = img.filter(ImageFilter.MaxFilter(size=3))
        img.show()
        updateTheProcessedImage(maxFilter)
        maxFilter.show(title='Max Filter Image')

    def laplacianFiltering(self):
        # Load the image and Convert the image to grayscale
        img = cv.imread(ProjectGlobals.FirstSelectedImage, 0)
        # Create the Laplacian kernel
        kernel = np.array([[1, 1, 1], [1, -8, 1], [1, 1, 1]])
        # Apply the Laplacian filter to the image
        laplacian = cv.filter2D(img, cv.CV_64F, kernel)
        # show the resulting image
        cv.imshow('Image Before laplacian filter', img)
        cv.imshow('Image after laplacian filter', laplacian)
        updateTheProcessedImage(laplacian)
        cv.waitKey(0)

    def laplacianOfGaussianFiltering(self):
        # Load the image and Convert the image to grayscale
        img = cv.imread(ProjectGlobals.FirstSelectedImage, 0)
        # Apply Gaussian Blur
        blur_image_gaussian = cv.GaussianBlur(img, (3, 3), 0)
        # Apply the Laplacian filter to the image
        laplacian = cv.Laplacian(blur_image_gaussian, cv.CV_64F, 3)
        # show the resulting image
        cv.imshow('Image Before LOG filter', img)
        cv.imshow('Image after laplacian filter', laplacian)
        updateTheProcessedImage(laplacian)
        cv.waitKey(0)

    def prewittFiltering(self):
        # Load the image and Convert the image to grayscale
        img = cv.imread(ProjectGlobals.FirstSelectedImage, 0)
        # Apply Gaussian Blur
        blur_image_gaussian = cv.GaussianBlur(img, (3, 3), 0)
        # Create the Prewitt kernels
        kernelX = np.array([[-1, 0, 1], [-1, 0, 1], [-1, 0, 1]])
        kernelY = np.array([[-1, -1, -1], [0, 0, 0], [1, 1, 1]])
        # Apply the Prewitt operator to the image
        prewittX = cv.filter2D(blur_image_gaussian, -1, kernelX)
        prewittY = cv.filter2D(blur_image_gaussian, -1, kernelY)
        # Combine the gradient magnitudes along the two axes
        prewitt = prewittX + prewittY
        # show the resulting image
        # cv.imshow('PrewittX', prewittX)
        # cv.imshow('PrewittY', prewittY)
        cv.imshow('After prewitt filter', prewitt)
        updateTheProcessedImage(prewitt)
        cv.waitKey(0)

    def sobelFiltering(self):
        # Load the image and Convert the image to grayscale
        img = cv.imread(ProjectGlobals.FirstSelectedImage, 0)
        # Apply the Sobel operator to the image
        sobelX = cv.Sobel(img, cv.CV_64F, 1, 0, ksize=3)
        sobelY = cv.Sobel(img, cv.CV_64F, 0, 1, ksize=3)
        # Combine the gradient magnitudes along the two axes
        sobel = sobelX + sobelY
        # Save the resulting image
        # cv.imshow('sobelX', sobelX)
        # cv.imshow('sobelY', sobelY)
        cv.imshow('After sobel filter', sobel)
        updateTheProcessedImage(sobel)
        cv.waitKey(0)

    def weightedFiltering(self):
        # Load the image and Convert the image to grayscale
        img = cv.imread(ProjectGlobals.FirstSelectedImage)
        # Create the Laplacian kernel
        kernel = np.array([[1, 2, 1], [2, 4, 2], [1, 2, 1]]) / 16
        # Apply the Laplacian filter to the image
        weighted = cv.filter2D(img, -1, kernel)
        # show the resulting image
        cv.imshow('Image Before weighted filter', img)
        cv.imshow('Image after weighted filter', weighted)
        updateTheProcessedImage(weighted)
        cv.waitKey(0)


class FilteringFactory():
    global filteringFunctions
    filteringFunctions = FilteringFunctions()

    @staticmethod
    def startProcessing(operation):
        if (operation == FilteringOperations.averageFilter.value):
            filteringFunctions.averageFiltering()
        if (operation == FilteringOperations.gaussianFilter.value):
            filteringFunctions.gaussianFiltering()
        if (operation == FilteringOperations.medianFilter.value):
            filteringFunctions.medianFiltering()
        if (operation == FilteringOperations.minFilter.value):
            filteringFunctions.minFiltering()
        if (operation == FilteringOperations.maxFilter.value):
            filteringFunctions.maxFiltering()
        if (operation == FilteringOperations.laplacianFilter.value):
            filteringFunctions.laplacianFiltering()
        if (operation == FilteringOperations.laplacianOfGaussianFilter.value):
            filteringFunctions.laplacianOfGaussianFiltering()
        if (operation == FilteringOperations.prewittFilter.value):
            filteringFunctions.prewittFiltering()
        if (operation == FilteringOperations.sobelFilter.value):
            filteringFunctions.sobelFiltering()
        if (operation == FilteringOperations.weightedFilter.value):
            filteringFunctions.weightedFiltering()
"""
from PIL import Image
import numpy as np
import cv2 as cv
from functions.save_image_function import updateTheProcessedImage
from utils.project_enums import FilteringOperations
from utils.project_globals import ProjectGlobals
# Protanopia Filter
def rgb_to_hsv(r, g, b):
    rgb = np.uint8([[[r, g, b]]])
    hsv = cv.cvtColor(rgb, cv.COLOR_RGB2HSV)
    return hsv[0][0][0] / 179.0, hsv[0][0][1] / 255.0, hsv[0][0][2] / 255.0

def hsv_to_rgb(h, s, v):
    hsv = np.uint8([[[h * 179, s * 255, v * 255]]])
    rgb = cv.cvtColor(hsv, cv.COLOR_HSV2RGB)
    return int(rgb[0][0][0]), int(rgb[0][0][1]), int(rgb[0][0][2])

def is_dominant_color(r, g, b, dominant_color):
    if dominant_color == "red":
        return r > g and r > b
    elif dominant_color == "green":
        return g > r and g > b
    return False

def adjust_hsl(h, s, l, dominant):
    if dominant:
        h = (h + 0.3) % 1.0
        s = min(s * 1.1, 1.0)
        l = min(l + 0.25, 1.0)
    else:
        s = min(s * 1.1, 1.0)
        l = min(l * 1.1, 1.0)

    return h, s, l

def protanopiaFiltering(image_path, output_path):
    if isinstance(image_path, np.ndarray):
        img = Image.fromarray(image_path)
    else:
        img = Image.open(image_path)
    img = img.convert('RGB')
    pixels = img.load()
    width, height = img.size

    red_count = 0
    green_count = 0
    for y in range(height):
        for x in range(width):
            r, g, b = pixels[x, y]
            if r > g and r > b:
                red_count += 1
            elif g > r and g > b:
                green_count += 1

    dominant_color = "red" if red_count > green_count else "green"

    for y in range(height):
        for x in range(width):
            r, g, b = pixels[x, y]
            h, s, v = rgb_to_hsv(r, g, b)
            dominant = is_dominant_color(r, g, b, dominant_color)
            h, s, v = adjust_hsl(h, s, v, dominant)
            r, g, b = hsv_to_rgb(h, s, v)
            pixels[x, y] = (r, g, b)

    img.save(output_path)
    display_image(output_path)

# Deuteranopia Filter
CVDMatrix = {
    "Deuteranope": [
        1.0, 0.0, 0.0,
        0.494207, 0.0, 1.24827,
        0.0, 0.0, 1.0
    ]
}

def daltonize(image_path, output_path):
    cvd = CVDMatrix["Deuteranope"]
    cvd_a, cvd_b, cvd_c = cvd[0], cvd[1], cvd[2]
    cvd_d, cvd_e, cvd_f = cvd[3], cvd[4], cvd[5]
    cvd_g, cvd_h, cvd_i = cvd[6], cvd[7], cvd[8]

    if isinstance(image_path, np.ndarray):
        img = Image.fromarray(image_path)
    else:
        img = Image.open(image_path)
    img = img.convert('RGB')
    np_img = np.array(img, dtype=float)

    for y in range(np_img.shape[0]):
        for x in range(np_img.shape[1]):
            r, g, b = np_img[y, x, 0], np_img[y, x, 1], np_img[y, x, 2]

            L = (17.8824 * r) + (43.5161 * g) + (4.11935 * b)
            M = (3.45565 * r) + (27.1554 * g) + (3.86714 * b)
            S = (0.0299566 * r) + (0.184309 * g) + (1.46709 * b)

            l = (cvd_a * L) + (cvd_b * M) + (cvd_c * S)
            m = (cvd_d * L) + (cvd_e * M) + (cvd_f * S)
            s = (cvd_g * L) + (cvd_h * M) + (cvd_i * S)

            R = (0.0809444479 * l) + (-0.130504409 * m) + (0.116721066 * s)
            G = (-0.0102485335 * l) + (0.0540193266 * m) + (-0.113614708 * s)
            B = (-0.000365296938 * l) + (-0.00412161469 * m) + (0.693511405 * s)

            R = r - R
            G = g - G
            B = b - B

            RR = (0.0 * R) + (0.0 * G) + (0.0 * B)
            GG = (0.7 * R) + (1.0 * G) + (0.0 * B)
            BB = (0.7 * R) + (0.0 * G) + (1.0 * B)

            R = RR + r
            G = GG + g
            B = BB + b

            R = max(0, min(255, R))
            G = max(0, min(255, G))
            B = max(0, min(255, B))

            np_img[y, x, 0] = R
            np_img[y, x, 1] = G
            np_img[y, x, 2] = B

    corrected_img = Image.fromarray(np_img.astype('uint8'), 'RGB')
    corrected_img.save(output_path)
    display_image(output_path)

# Tritanopia Filter
def tritanopiaFiltering(image_path, output_path):
    CVDMatrix = {
        "Tritanope": [
            1.0, 0.0, 0.0,
            0.0, 1.0, 0.0,
            -0.395913, 0.801109, 0.0
        ]
    }

    cvd = CVDMatrix["Tritanope"]
    cvd_a, cvd_b, cvd_c = cvd[0], cvd[1], cvd[2]
    cvd_d, cvd_e, cvd_f = cvd[3], cvd[4], cvd[5]
    cvd_g, cvd_h, cvd_i = cvd[6], cvd[7], cvd[8]

    if isinstance(image_path, np.ndarray):
        img = Image.fromarray(image_path)
    else:
        img = Image.open(image_path)
    img = img.convert('RGB')
    np_img = np.array(img, dtype=float)

    for y in range(np_img.shape[0]):
        for x in range(np_img.shape[1]):
            r, g, b = np_img[y, x, 0], np_img[y, x, 1], np_img[y, x, 2]

            L = (17.8824 * r) + (43.5161 * g) + (4.11935 * b)
            M = (3.45565 * r) + (27.1554 * g) + (3.86714 * b)
            S = (0.0299566 * r) + (0.184309 * g) + (1.46709 * b)

            l = (cvd_a * L) + (cvd_b * M) + (cvd_c * S)
            m = (cvd_d * L) + (cvd_e * M) + (cvd_f * S)
            s = (cvd_g * L) + (cvd_h * M) + (cvd_i * S)

            R = (0.0809444479 * l) + (-0.130504409 * m) + (0.116721066 * s)
            G = (-0.0102485335 * l) + (0.0540193266 * m) + (-0.113614708 * s)
            B = (-0.000365296938 * l) + (-0.00412161469 * m) + (0.693511405 * s)

            R = r - R
            G = g - G
            B = b - B

            RR = (0.0 * R) + (0.0 * G) + (0.0 * B)
            GG = (0.7 * R) + (1.0 * G) + (0.0 * B)
            BB = (0.7 * R) + (0.0 * G) + (1.0 * B)

            R = RR + r
            G = GG + g
            B = BB + b

            R = max(0, min(255, R))
            G = max(0, min(255, G))
            B = max(0, min(255, B))

            np_img[y, x, 0] = R
            np_img[y, x, 1] = G
            np_img[y, x, 2] = B

    corrected_img = Image.fromarray(np_img.astype('uint8'), 'RGB')
    corrected_img.save(output_path)
    display_image(output_path)

def display_image(image_path):
    img = cv.imread(image_path)
    cv.imshow('Filtered Image', img)
    cv.waitKey(0)  # Wait for a key press

# FilteringFactory and FilteringOperations Classes
class FilteringFunctions():
    def protanopiaFiltering(self):
        image_path = img = cv.imread(ProjectGlobals.FirstSelectedImage)
        output_path = "protanopia_filtered_image.png"
        protanopiaFiltering(image_path, output_path)
        cv.imshow('Protanopia', "protanopia_filtered_image.png")
        updateTheProcessedImage("protanopia_filtered_image.png")
        cv.waitKey(0)
    
    def deuteranopiaFiltering(self):
        image_path = img = cv.imread(ProjectGlobals.FirstSelectedImage)
        output_path = "deuteranopia_filtered_image.png"
        daltonize(image_path, output_path)
        cv.imshow('Deuteranopia', "deuteranopia_filtered_image.png")
        updateTheProcessedImage("deuteranopia_filtered_image.png")
        cv.waitKey(0)
    
    def tritanopiaFiltering(self):
        image_path = img = cv.imread(ProjectGlobals.FirstSelectedImage)
        output_path = "tritanopia_filtered_image.png"
        tritanopiaFiltering(image_path, output_path)
        cv.imshow('Tritanopia', "tritanopia_filtered_image.png")
        updateTheProcessedImage("tritanopia_filtered_image.png")
        cv.waitKey(0)

class FilteringFactory():
    global filteringFunctions
    filteringFunctions = FilteringFunctions()

    @staticmethod
    def startProcessing(operation):
        if operation == FilteringOperations.protanopiaFilter.value:
            filteringFunctions.protanopiaFiltering()
        if operation == FilteringOperations.deuteranopiaFilter.value:
            filteringFunctions.deuteranopiaFiltering()
        if operation == FilteringOperations.tritanopiaFilter.value:
            filteringFunctions.tritanopiaFiltering()
<<<<<<< HEAD

class FilteringOperations(Enum):
    protanopiaFilter = 1
    deuteranopiaFilter = 2
    tritanopiaFilter = 3
"
=======
>>>>>>> d4eef7b9b0b8f57029432187e8fc8fcba22989a5
