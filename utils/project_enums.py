from enum import Enum

class MainOperations(Enum):
    filtering = "Filtering"
    resizeImage = "Resize image"

class FilteringOperations(Enum):
    protanopiaFilter = "PROTONOPIA"
    deuteranopiaFilter = "DEUTERANOPIA"
    tritanopiaFilter = "TRITANOPIA"