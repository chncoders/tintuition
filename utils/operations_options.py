from flet import dropdown
from utils.project_enums import FilteringOperations, MainOperations

class OperationsOptions():
    mainOperations = [
        dropdown.Option(MainOperations.filtering.value),
        dropdown.Option(MainOperations.resizeImage.value),
    ]
    filteringOperations = [
        dropdown.Option(FilteringOperations.protanopiaFilter.value),
        dropdown.Option(FilteringOperations.deuteranopiaFilter.value),
        dropdown.Option(FilteringOperations.tritanopiaFilter.value),
    ]
