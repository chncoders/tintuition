**Problem Statement**
___Title___: _Enhancing Color Perception for Colorblind Individuals through Image Processing_


___Objective___:

The primary objective of this project is to develop a user-friendly application designed to enhance image colors for individuals with color vision deficiencies. This application will use the Daltonization algorithm and the Color Blind Filter Service(CBFS) to tailor color adjustments to their specific type of color blindness.

Other objectives include learning flet, exploring several other interesting libraries and understanding research papers 


___Problem Overview___:

Colorblindness, or color vision deficiency (CVD), affects approximately 1 in 12 men and 1 in 200 women worldwide. This condition can make it challenging for affected individuals to differentiate between certain colors, impacting their daily activities and overall quality of life. Traditional solutions, such as color-corrective lenses, are often expensive and not universally effective.


___Solution___:

We propose the development of an accessible and user-friendly mobile application that leverages Python for image processing and Flet for the user interface. The app will allow users to scan or upload images, which will then be processed using the Daltonization algorithm to enhance color differentiation based on the type of color blindness.


___Key Features___:

**Image Scanning and Uploading**: Users can scan images using their mobile device's camera or upload images from their gallery.  
**Color Blindness Type Selection**: Users can specify their type of color blindness, allowing the app to apply the appropriate color correction.  
**Real-time Preview**: The app will provide a real-time preview of the enhanced image, allowing users to see the improvements immediately.  
**Save and Share**: Users can save the enhanced images to their device or share them directly from the app.  


___Impact___:

By providing an effective and affordable solution to enhance color perception, this app aims to improve the visual experience for colorblind individuals, helping them to better perceive and interact with the world around them.

