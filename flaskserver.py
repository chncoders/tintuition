from flask import Flask, request, send_file, jsonify
from PIL import Image
import requests
import os
import subprocess
app = Flask(__name__)
GITLAB_REPO_URL = "https://gitlab.com/chncoders/tintuition"
SCRIPT_PATH = 'trial.py'
UPLOAD_FOLDER = 'uploads'
PROCESSED_FOLDER = 'processed'
def fetch_script():
    response = requests.get(GITLAB_REPO_URL)
    with open(SCRIPT_PATH, 'wb') as f:
        f.write(response.content)
@app.route('/process-image', methods=['POST'])
def process_image():
    if 'file' not in request.files:
        return 'No file part', 400
    file = request.files['file']
    if file.filename == '':
        return 'No selected file', 400
    os.makedirs(UPLOAD_FOLDER, exist_ok=True)
    os.makedirs(PROCESSED_FOLDER, exist_ok=True)
    input_path = os.path.join(UPLOAD_FOLDER, file.filename)
    output_path = os.path.join(PROCESSED_FOLDER, 'processed_' + file.filename)
    file.save(input_path)
    fetch_script()
    subprocess.run(['python', SCRIPT_PATH, input_path, output_path])
    return jsonify({'url': f'http://{request.host}/{output_path}'})

@app.route('/<path:filename>', methods=['GET'])
def serve_file(filename):
    return send_file(filename)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)
